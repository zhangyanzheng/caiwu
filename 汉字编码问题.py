#-*- coding:gbk -*-
import openpyxl
wb = openpyxl.Workbook()
sheet = wb.active
sheet.title = '净值表'.decode('gbk')
sheet['A1'] = '净值日期'.decode('gbk').encode('utf-8')
sheet['B1'] = '单位净值'.decode('gbk').encode('utf-8')
sheet['C1'] = '累计净值'.decode('gbk').encode('utf-8')

wb.save(r'C:\Users\Administrator\Desktop\Marvel.xlsx')

###-*- coding:gbk -*- 文件内的汉字使用gbk编码
##'净值表'.decode('gbk') = unicode编码
##'净值日期'.decode('gbk').encode('utf-8') =unicode编码.encode('utf-8') = utf-8编码

###在windows命令提示符中，正常显示gbk、unicode编码
###在pycharm编辑器里，正常显示utf-8、unicode编码


##################参考资料：################
The error is occuring due to the code snippet

u'Десктопы полно'.decode('utf-8')
The prefix 'u' makes a string a Unicode string. The Unicode string is not actually encoded in anyway and is already in the decoded form.

For example,

>>> s='Десктопы полно'
>>> u=u'Десктопы полно'
>>> s
'\xd0\x94\xd0\xb5\xd1\x81\xd0\xba\xd1\x82\xd0\xbe\xd0\xbf\xd1\x8b \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xbd\xd0\xbe'
>>> u
u'\u0414\u0435\u0441\u043a\u0442\u043e\u043f\u044b \u043f\u043e\u043b\u043d\u043e'
>>> s.decode('utf-8')
u'\u0414\u0435\u0441\u043a\u0442\u043e\u043f\u044b \u043f\u043e\u043b\u043d\u043e'
>>> u.encode('utf-8')
'\xd0\x94\xd0\xb5\xd1\x81\xd0\xba\xd1\x82\xd0\xbe\xd0\xbf\xd1\x8b \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xbd\xd0\xbe'
We can see that s == u.encode('utf-8')

For further detailed explanation as to why, you can go through http://pythoncentral.io/python-unicode-encode-decode-strings-python-2x/

So, basically, the Unicode string has to be encoded and not decoded, i.e. ,

u'Десктопы полно'.encode('utf-8')


https://stackoverflow.com/questions/38086195/unicodedecodeerror-when-write-to-excel-using-python